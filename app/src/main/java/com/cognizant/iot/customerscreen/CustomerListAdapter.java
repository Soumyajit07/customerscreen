package com.cognizant.iot.customerscreen;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.iot.customerscreen.Single_Customer_Screen.CustomerDetailScreen;

import java.util.ArrayList;

/**
 * Created by Guest_User on 16/06/17.
 */

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.CustomerListViewHolder> {

    Context context;
    ArrayList<CustomerModel> customerModelArrayList;

    public CustomerListAdapter(Context context, ArrayList<CustomerModel> customerModelArrayList) {
        this.context = context;
        this.customerModelArrayList = customerModelArrayList;
    }


    @Override
    public CustomerListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_customer_view_in_list,parent,false);
        CustomerListViewHolder customerListViewHolder=new CustomerListViewHolder(view);
        return customerListViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomerListViewHolder holder, int position) {
        CustomerModel customerModel=customerModelArrayList.get(position);
        Log.e("cusomermipmap",String.valueOf(customerModel.getCustomer_image_id()));
        holder.customer_image.setImageResource(customerModel.getCustomer_image_id());
        holder.customer_image.setTag(customerModel.getCustomer_image_id());
        holder.customer_name.setText(customerModel.getCustomer_name());
        holder.customer_type.setText(customerModel.getCustomer_type());
    }

    @Override
    public int getItemCount() {
        return customerModelArrayList.size();
    }

    public class CustomerListViewHolder extends RecyclerView.ViewHolder{

        CircleImageView customer_image;
        TextView customer_name;
        TextView customer_type;
        ArrayList<String> details=new ArrayList<>();

        public CustomerListViewHolder(View itemView) {
            super(itemView);
            customer_image=(CircleImageView) itemView.findViewById(R.id.customer_imageview);
            customer_name=(TextView) itemView.findViewById(R.id.customer_name);
            customer_type=(TextView) itemView.findViewById(R.id.customer_type);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    details.add(0,String.valueOf(customer_image.getTag()));
                    Log.e("customerimageresource",String.valueOf(customer_image.getTag()));
                    details.add(1,customer_name.getText().toString());
                    details.add(2,customer_type.getText().toString());
                    context.startActivity(new Intent(context, CustomerDetailScreen.class).putStringArrayListExtra("details",details).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            });
        }
    }
}
