package com.cognizant.iot.customerscreen.Single_Customer_Screen;

/**
 * Created by Guest_User on 20/06/17.
 */

public class OfferModel {
    String offerName;
    String couponCode;
    int validity;

    public OfferModel(String offerName, String couponCode, int validity) {
        this.offerName = offerName;
        this.couponCode = couponCode;
        this.validity = validity;
    }

    public String getOfferName() {
        return offerName;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public int getValidity() {
        return validity;
    }
}
