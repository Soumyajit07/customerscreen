package com.cognizant.iot.customerscreen;

/**
 * Created by Guest_User on 16/06/17.
 */

public class CustomerModel {
    String customer_name;
    String customer_type;
    int customer_image_id;

    public CustomerModel() {
    }

    public CustomerModel(String customer_name, String customer_type, int customer_image_id) {
        this.customer_name = customer_name;
        this.customer_type = customer_type;
        this.customer_image_id = customer_image_id;
    }


    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public void setCustomer_image_id(int customer_image_id) {
        this.customer_image_id = customer_image_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public int getCustomer_image_id() {
        return customer_image_id;
    }
}