package com.cognizant.iot.customerscreen.Single_Customer_Screen;

/**
 * Created by Guest_User on 19/06/17.
 */

public class ProductModel {
    int productimageid;
    String productname;

    public ProductModel(int productimageid, String productname) {
        this.productimageid = productimageid;
        this.productname = productname;
    }

    public int getProductimageid() {
        return productimageid;
    }

    public String getProductname() {
        return productname;
    }
}
