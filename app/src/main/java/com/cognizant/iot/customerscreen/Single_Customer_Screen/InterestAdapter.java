package com.cognizant.iot.customerscreen.Single_Customer_Screen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.iot.customerscreen.CircleImageView;
import com.cognizant.iot.customerscreen.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 21/06/17.
 */

public class InterestAdapter extends RecyclerView.Adapter<InterestAdapter.InterestViewHolder> {

    Context context;
    ArrayList<InterstModel> interstModels;

    public InterestAdapter(Context context, ArrayList<InterstModel> interstModels) {
        this.context = context;
        this.interstModels = interstModels;
    }

    @Override
    public InterestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_interest_layout,parent,false);
        InterestViewHolder interestViewHolder=new InterestViewHolder(view);
        return interestViewHolder;
    }

    @Override
    public void onBindViewHolder(InterestViewHolder holder, int position) {
        InterstModel interstModel=interstModels.get(position);
        holder.interestimage.setImageResource(interstModel.getImageid());
    }

    @Override
    public int getItemCount() {
        return interstModels.size();
    }

    public class InterestViewHolder extends RecyclerView.ViewHolder{

        CircleImageView interestimage;

        public InterestViewHolder(View itemView) {

            super(itemView);
            interestimage=(CircleImageView) itemView.findViewById(R.id.interstpic);
        }
    }
}
