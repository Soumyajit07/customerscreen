package com.cognizant.iot.customerscreen.Single_Customer_Screen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.cognizant.iot.customerscreen.CircleImageView;
import com.cognizant.iot.customerscreen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class CustomerDetailScreen extends AppCompatActivity {

    CircleImageView customer_image;
    TextView customer_name,customer_type;
    RecyclerView past_history,offers,interestsrecyclerview;
    ArrayList<ProductModel> productmodels;
    ArrayList<OfferModel> offermodels;
    ArrayList<InterstModel> interestmodels;
    ArrayList<String> details;
    int[] products={R.mipmap.tshirt,R.mipmap.dinnerware,R.mipmap.watch,R.mipmap.wallclock};
    int[] interests={R.drawable.ic_fashion,R.drawable.ic_drinkware,R.drawable.ic_furniture,R.drawable.ic_grooming,R.drawable.ic_latop};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail_screen);
        Intent intent=getIntent();
        details=intent.getStringArrayListExtra("details");
        customer_image=(CircleImageView) findViewById(R.id.customer_imageview_detailscreen);
        Log.e("image",details.get(0));
        customer_image.setImageResource(this.getResources().getIdentifier(details.get(0),"string",this.getPackageName()));
        customer_name=(TextView) findViewById(R.id.customer_name_detailscreen);
        customer_name.setText(details.get(1));
        customer_type=(TextView) findViewById(R.id.customer_type_name_detailscreen);
        customer_type.setText(details.get(2));
        past_history=(RecyclerView) findViewById(R.id.past_history_recyclerview);
        offers=(RecyclerView) findViewById(R.id.offers_recyclerview);
        interestsrecyclerview=(RecyclerView) findViewById(R.id.likes_recyclerview);
        setproductrecyclerview();
        setofferrecyclerview();
        setinterestrecyclerview();
    }



    private void setproductrecyclerview() {
        productmodels=new ArrayList<>();
        try
        {
            JSONObject product=new JSONObject(loadJSONFromAsset("products.json"));
            JSONArray productArray=product.getJSONArray("products");
            for(int i=0;i<productArray.length();i++)
            {
                JSONObject singleproduct=productArray.getJSONObject(i);
                productmodels.add(new ProductModel(products[i],singleproduct.getString("name")));
            }
            Log.e("productmodels",productmodels.size()+" "+productmodels.toString());
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        ProductAdapter productAdapter=new ProductAdapter(CustomerDetailScreen.this,productmodels);
        past_history.setLayoutManager(new LinearLayoutManager(CustomerDetailScreen.this,LinearLayoutManager.HORIZONTAL,false));
        past_history.setHasFixedSize(true);
        past_history.setAdapter(productAdapter);

    }

    private void setofferrecyclerview() {
        offermodels=new ArrayList<>();
        try
        {
            JSONObject offer=new JSONObject(loadJSONFromAsset("offers.json"));
            JSONArray offerArray=offer.getJSONArray("offers");
            for(int i=0;i<offerArray.length();i++)
            {
                JSONObject singleoffer=offerArray.getJSONObject(i);
                offermodels.add(new OfferModel(singleoffer.getString("name"),singleoffer.getString("coupon code"),singleoffer.getInt("validity")));
            }
            Log.e("offermodels",offermodels.size()+" "+offermodels.toString());
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        OfferAdapter offerAdapter=new OfferAdapter(CustomerDetailScreen.this,offermodels);
        offers.setLayoutManager(new LinearLayoutManager(CustomerDetailScreen.this,LinearLayoutManager.HORIZONTAL,false));
        offers.setHasFixedSize(true);
        offers.setAdapter(offerAdapter);
    }

    private void setinterestrecyclerview() {
        interestmodels=new ArrayList<>();
        for(int i=0;i<interests.length;i++)
        {
            InterstModel singleinterest=new InterstModel(interests[i]);
            interestmodels.add(singleinterest);
        }
        InterestAdapter interestAdapter=new InterestAdapter(CustomerDetailScreen.this,interestmodels);
        interestsrecyclerview.setLayoutManager(new LinearLayoutManager(CustomerDetailScreen.this,LinearLayoutManager.HORIZONTAL,false));
        interestsrecyclerview.setHasFixedSize(true);
        interestsrecyclerview.setAdapter(interestAdapter);
    }


    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }
}
