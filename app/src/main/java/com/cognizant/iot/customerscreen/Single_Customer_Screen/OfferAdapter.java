package com.cognizant.iot.customerscreen.Single_Customer_Screen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.iot.customerscreen.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 20/06/17.
 */

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferViewHolder>{

    Context context;
    ArrayList<OfferModel> offerModels;

    public OfferAdapter(Context context, ArrayList<OfferModel> offerModels) {
        this.context = context;
        this.offerModels = offerModels;
    }

    @Override
    public OfferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_offer_layout,parent,false);
        OfferViewHolder offerViewHolder=new OfferViewHolder(view);
        return offerViewHolder;
    }

    @Override
    public void onBindViewHolder(OfferViewHolder holder, int position) {
        OfferModel offerModel=offerModels.get(position);
        holder.offer.setText(offerModel.getOfferName());
        holder.couponCode.setText(offerModel.getCouponCode());
        holder.validity.setText(String.valueOf(offerModel.getValidity()));
    }

    @Override
    public int getItemCount() {
        return offerModels.size();
    }

    public class OfferViewHolder extends RecyclerView.ViewHolder{

        TextView offer;
        TextView couponCode;
        TextView validity;

        public OfferViewHolder(View itemView) {
            super(itemView);
            offer=(TextView) itemView.findViewById(R.id.offer_name);
            couponCode=(TextView) itemView.findViewById(R.id.offer_couponcode);
            validity=(TextView) itemView.findViewById(R.id.offer_validity);
        }
    }
}
