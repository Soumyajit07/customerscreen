package com.cognizant.iot.customerscreen;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class CustomerList extends AppCompatActivity {

    RecyclerView customer_list_recyclerview;
    ArrayList<CustomerModel> customers;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        customer_list_recyclerview=(RecyclerView) findViewById(R.id.customer_list_recyclerview);
        setcustomerscreen();
    }

    private void setcustomerscreen() {
        customers=new ArrayList<>();
        try
        {
            JSONObject customerJson=new JSONObject(loadJSONFromAsset("customers.json"));
            JSONArray customerArray=customerJson.getJSONArray("customers");
            for(int i=0;i<customerArray.length();i++)
            {
                JSONObject singlecustomer=customerArray.getJSONObject(i);
                if(singlecustomer.getString("type").contains("Valuable"))
                {
                    CustomerModel customerModel=new CustomerModel(singlecustomer.getString("name"),singlecustomer.getString("type"),R.mipmap.customer3);
                    customers.add(customerModel);
                }
            }
            for(int i=0;i<customerArray.length();i++)
            {
                JSONObject singlecustomer=customerArray.getJSONObject(i);
                if(singlecustomer.getString("type").contains("Special"))
                {
                    CustomerModel customerModel=new CustomerModel(singlecustomer.getString("name"),singlecustomer.getString("type"),R.mipmap.customer3);
                    customers.add(customerModel);
                }
            }
            for(int i=0;i<customerArray.length();i++)
            {
                JSONObject singlecustomer=customerArray.getJSONObject(i);
                if(singlecustomer.getString("type").contains("Normal"))
                {
                    CustomerModel customerModel=new CustomerModel(singlecustomer.getString("name"),singlecustomer.getString("type"),R.mipmap.customer3);
                    customers.add(customerModel);
                }
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        CustomerListAdapter customerAdapter=new CustomerListAdapter(CustomerList.this,customers);
        customer_list_recyclerview.setLayoutManager(new LinearLayoutManager(CustomerList.this,LinearLayoutManager.VERTICAL,false));
        customer_list_recyclerview.setHasFixedSize(true);
        customer_list_recyclerview.setAdapter(customerAdapter);
    }

    private String loadJSONFromAsset(String name) {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSON",json);
        return json;
    }
}




