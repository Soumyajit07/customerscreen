package com.cognizant.iot.customerscreen.Single_Customer_Screen;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.customerscreen.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 19/06/17.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    Context context;
    ArrayList<ProductModel> productModelArrayList;

    public ProductAdapter(Context context, ArrayList<ProductModel> productModelArrayList) {
        this.context = context;
        this.productModelArrayList = productModelArrayList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.single_product_layout,parent,false);
        ProductViewHolder productViewHolder=new ProductViewHolder(view);
        return productViewHolder;
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        ProductModel productModel=productModelArrayList.get(position);
        Log.e("gotmipmap",String.valueOf(productModel.getProductimageid()));
        holder.productimage.setImageResource(productModel.getProductimageid());
        holder.productname.setText(productModel.getProductname());
    }

    @Override
    public int getItemCount() {
        return productModelArrayList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        ImageView productimage;
        TextView productname;

        public ProductViewHolder(View itemView) {
            super(itemView);
            productimage=(ImageView) itemView.findViewById(R.id.product_image);
            productname=(TextView) itemView.findViewById(R.id.product_name);
        }
    }
}
